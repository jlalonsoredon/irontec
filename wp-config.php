<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'irontec' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Bjhd_RPfJ]q//*])2PqTh,r$6Q=?/v#z(DF|/6j-y%}+h9j&}8mzTcoUZ?kZIfIm' );
define( 'SECURE_AUTH_KEY',  'gOq:HP1Z-u~QNR/><raW4<NJS)exRs[*ZxSVb&pm<%rtsdf2N5`4N~0_xZ_WHTBV' );
define( 'LOGGED_IN_KEY',    'iLF1.Afl!*=-f8YEnI$X88&+E,+%j0()U*t`<lIWH:{%[Pn&Y=4}/o-qMZny3cFL' );
define( 'NONCE_KEY',        '&#m)n:4tDn5n@LrWo<<:/@npZ;BOfj_L{uD7OoJ_y#9O^^&R$QX@%#m?$JBt?fV)' );
define( 'AUTH_SALT',        'F%{Ea`1]g!7o.8h`Xv~wQR3,>P/2~LR5{_gQ2g`nWAD#KA}uM2_z&kLArO}FQBy/' );
define( 'SECURE_AUTH_SALT', 'g>k=pzP@lrai =b[)*y&XD>S:nUz4~l9HA&C$edJ>yg)aRs!n&_J{v7T){f$7l&?' );
define( 'LOGGED_IN_SALT',   'o&;R(aX_bJB?85,JndmtBI4tS$0zPp:9WG]p)^WmGfErdlg,vFH;DM/UjoXRz^8N' );
define( 'NONCE_SALT',       'r?,&ww9hs]vt;V+@1(9q-2.u6m=_p>68ei=GpEV5cwXVT< Y W0|~`~&O!FPBkd`' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
