La solución que propongo es una adaptación de Yourl a Wordpress a través de un plugin.Para ello se ha instalado yourl en la raiz de Wordpress dentro de un directorio llamado sl. Esto se ha hecho de este modo para acortar lo más posible la URL ya que si se instalase dentro del plugin la URL acortado incluiría la ruta completa, dominio\wp-content\plugins\acortador-de-urls.La configuración inicial de se realiza en el directorio dominio/sl/user/config.php. En el que hay que editar los campos:/** MySQL database username */
define( 'YOURLS_DB_USER', 'username ' );

/** MySQL database password */
define( 'YOURLS_DB_PASS', 'password ' );

/** The name of the database for YOURLS */
define( 'YOURLS_DB_NAME', 'name-database' );

/** MySQL hostname.
 ** If using a non standard port, specify it like 'hostname:port', eg. 'localhost:9999' or '127.0.0.1:666' */
define( 'YOURLS_DB_HOST', 'hostname' );

/** MySQL tables prefix */
define( 'YOURLS_DB_PREFIX', 'yourl_' );

define( 'YOURLS_SITE', 'DOMINIO/sl' );

$yourls_user_passwords = array(
	'username' => 'passwords' /* Password encrypted by YOURLS */ ,
	// You can have one or more 'login'=>'password' lines
	);

El plugin llamado acortador-de-urls crea una entrada en el menú de administración llamado "Administrador de enlaces" que carga el panel de yourl.El plugin también incluye un sortcode [listlinks] para imprimir el listado de enlaces en cualquier página y que sea visible desde el front.