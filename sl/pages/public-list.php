<?php

// Make sure we're in YOURLS context
if( !defined( 'YOURLS_ABSPATH' ) ) {
	echo "Try using a URL without the /pages/ part";
	die();
}

// Display page content. Any PHP, HTML and YOURLS function can go here.
$url = YOURLS_SITE . '/public-list';

yourls_html_head( 'public-list', 'listado de enlaces' );

?>

<p>listado de enlaces <?php echo $url; ?></p>

<?php

yourls_html_footer();

